@extends('front-end.master')
@section('body')
    <section class="cat_product_area section_gap">
        <div class="container">
            <div class="row flex-row-reverse">
                <div class="col-lg-12">
                    <div class="latest_product_inner">
                        <div class="row">
                            @foreach($information as $info)
                                <div class="col-lg-4 col-md-6">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <img
                                                class="card-img"
                                                src="{{asset($info->product_image)}}"
                                                alt=""
                                            />

                                        </div>
                                        <div class="product-btm">
                                            <a href="{{route('product.productDetails',$info->id)}}" class="d-block">
                                               <h4>Name:</h4> {{$info->product_name}}
                                            </a>
                                            <div class="mt-3">
                                             <h4>Description:</h4>   {{$info->product_desc}}
                                            </div>
                                            <div class="mt-3">
                                               <h4>Product Price(tk)</h4> {{$info->product_price}}
                                            </div>
                                            <div class="mt-3">
                                               <h4>Product Quantity(kg)</h4> {{$info->product_quantity}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
