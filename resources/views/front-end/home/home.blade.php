@extends('front-end.master')
@section('title')
    Home
@endsection

@section('body')
    <!--================Home Banner Area =================-->
    <section class="home_banner_area mb-40">
        <div class="banner_inner d-flex">
            <div class="container">
                <div class="banner_content row">
                    <div class="col-lg-12 ">
                        <h3 class="text-danger mt-4"><span>One Stop</span> Agribusiness <br />And Agriculture <span>Service</span></h3>
                        <h4>Fowl saw dry which a above together place.</h4>
                        <a class="main_btn mt-40" href="{{route('addinformation')}}">Add a Product</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

    <section class="blog-area section-gap">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="main_title">
                        <h2><span>latest upload information</span></h2>
                        <p></p>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach($newInformations as $newInformation)
                <div class="col-lg-4 col-md-6">
                    <div class="single-blog">
                        <div class="thumb">
                            <img class="img-fluid" src="{{asset($newInformation->info_image)}}" alt="" width="400">
                        </div>
                        <div class="short_details">
                            <div class="meta-top d-flex">
                                <a href="#">{{$newInformation->info_name}}</a>
                            </div>
                            <div class="text-wrap">
                           <p>{{$newInformation->info_desc}}</p>
                            </div>
                            <a href="#" class="blog_btn">Learn More <span class="ml-2 ti-arrow-right"></span></a>
                        </div>
                    </div>
                </div>
                 @endforeach

            </div>
        </div>
    </section>
    <!--================ End Blog Area =================-->

    <!--================ Start Blog Area =================-->
    <section class="blog-area section-gap">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="main_title">
                        <h2><span>latest upload</span></h2>
                        <p></p>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach($newProducts as $newProduct)
                <div class="col-lg-4 col-md-6">
                    <div class="single-blog">
                        <div class="thumb">
                            <img class="img-fluid" src="{{asset($newProduct->product_image)}}" alt="" width="400">
                        </div>
                        <div class="short_details">
                            <div class="meta-top d-flex">
                                <a href="#"></a>
                                <a href="#"><i class="ti-comments-smiley"></i>2 Comments</a>
                            </div>
                            <a class="d-block">
                                <h4>{{$newProduct->product_name}}</h4>
                                <a href="{{route('product.productDetails',$newProduct->id)}}" class="d-block">
                            </a>
                            <div class="text-wrap">
                                <P>{{$newProduct->product_price}}</P>
                            </div>
                            <a href="#" class="blog_btn">Learn More <span class="ml-2 ti-arrow-right"></span></a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!--================ End Blog Area =================-->

@endsection
