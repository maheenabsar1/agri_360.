@extends('front-end.master')
@section('body')
    <section class="cat_product_area section_gap">
        <div class="container">
            <div class="row flex-row-reverse">
                <div class="col-lg-12">
                    <div class="latest_product_inner">
                        <div class="row">
                            @foreach($information as $info)
                                <div class="col-lg-4 col-md-6">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <img class="card-img" src="{{asset($info->info_image)}}" alt=""/>
                                        </div>
                                        <div class="product-btm">
                                            <a href="{{route('information.informationDetails',$info->id)}}" class="d-block">
                                                <h4>{{$info->info_name}}</h4>
                                            </a>
                                            <div class="mt-3">
                                                {{$info->info_desc}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
