@extends('front-end.farmer.Farmermaster')

@section('body')
    <div class="container-fluid">
        @if (Session::get('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{Session::get('message')}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row">

            <div style="" class="col-md-5">
                <h2>Add-Product</h2>
                <form action="{{route('newinformation')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div style="" class="form-group">
                        <label >Product name</label>
                        <input type="text" class="form-control" name="product_name"  placeholder="Information name">
                    </div>
                    <div class="form-group">
                        <label >Product Discription</label>
                        <textarea  class="form-control" name="product_desc" placeholder="Information Discription"></textarea>
                    </div>
                    <div class="form-group">
                        <label >Product Quantity</label>
                        <input type="number" class="form-control" name="product_quantity"  placeholder="Information name">
                    </div>
                    <div class="form-group">
                        <label >Product Price</label>
                        <input type="number" class="form-control" name="product_price"  placeholder="Information name">
                    </div>
                    <div class="form-group">
                        <label >Cell</label>
                        <input type="number" class="form-control" name="cell"  placeholder="cell">
                    </div>
                    <div class="form-group">
                        <label >Product Image</label>
                        <input type="file" class="form-control-file" name="product_image" >
                        <img src="" id="profile-img-tag" width="200px"/>
                    </div>
                    <div class="form-group ">
                        <label>Publication status</label>
                        <input type="radio" name="status" value="1" >
                        <label >Published</label>
                        <input type="radio" name="status" value="0" >
                        <label >Unpublished</label>
                    </div>
                    <button type="submit" name="btn" class="btn btn-primary">Add-information</button>
                    <button type="submit" name="btn" class="btn btn-primary">Logout</button>

                </form>

            </div>
        </div>

    </div>
@endsection
