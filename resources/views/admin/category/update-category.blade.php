@extends('admin.master')


@section('body')

    <div class="modal fade" id="edit{{$category->id}}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('update-category')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label >Category name</label>
                            <input type="text" class="form-control" value="{{$category->cat_name}}" name="cat_name"  placeholder="Category name">
                            <input type="hidden" class="form-control" value="{{$category->id}}" name="id">
                        </div>
                        <div class="form-group">
                            <label >Category Discription</label>
                            <textarea  class="form-control"   name="cat_desc" placeholder="Category Discription">{{$category->cat_desc}}</textarea>
                        </div>
                        <div class="form-group ">
                            <label>Publication status</label>
                            <input type="radio" name="status" value="1" {{$category->status== 1?'checked':''}}>
                            <label >Published</label>
                            <input type="radio" name="status" value="0" {{$category->status== 0?'checked':''}}>
                            <label >Unpublished</label>

                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" name="btn" class="btn btn-primary">Update Category</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @endsection
