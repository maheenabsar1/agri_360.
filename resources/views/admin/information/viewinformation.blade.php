
@extends('admin.master')

@section('body')

    <div class="container">
        @if (Session::get('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{Session::get('message')}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">All Information</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>S1</th>
                                    <th>Info Name</th>
                                    <th>Info Description</th>
                                    <th>Publication Status</th>
                                    <th>Image</th>
                                    <th class="">Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @php ($i = 1)
                                @foreach($information as $info)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$info->info_name}}</td>
                                        <td>{{$info->info_desc}}</td>
                                        <td><img src="{{ asset($info->info_image)}}" width="30%" alt=""></td>
                                        <td>{{$info->status==1?'published':'unpublished'}}</td>
                                        <td class="text-center">
                                            @if($info->status==1)
                                                <a href="{{route('published-information',['id'=>$info->id])}}" class="btn btn-sm btn-info">
                                                    <i class="fas fa-arrow-up"></i>
                                                </a>
                                            @else
                                                <a href="{{route('unpublished-information',['id'=>$info->id])}}" class="btn btn-sm btn-warning">
                                                    <i class="fas fa-arrow-down"></i>
                                                </a>
                                            @endif
                                            <a href=""  class="btn btn-sm btn-success" data-toggle="modal" data-target="#edit{{$info->id}}">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a href="{{route('delete-information',['id'=>$info->id])}}" class="btn btn-sm btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="edit{{$info->id}}">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit Information</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="#" method="post">
                                                        @csrf
                                                        <div class="form-group">
                                                            <label >Information name</label>
                                                            <input type="text" class="form-control" value="{{$info->info_name}}" name="info_name"  placeholder="Information name">
                                                            <input type="hidden" class="form-control" value="{{$info->id}}" name="id">
                                                        </div>
                                                        <div class="form-group">
                                                            <label >Information Description </label>
                                                            <textarea  class="form-control"   name="info_desc" placeholder="Information Description">{{$info->info_desc}}</textarea>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label>Publication status</label>
                                                            <input type="radio" name="status" value="1" {{$info->status== 1?'checked':''}}>
                                                            <label >Published</label>
                                                            <input type="radio" name="status" value="0" {{$info->status== 0?'checked':''}}>
                                                            <label >Unpublished</label>

                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="submit" name="btn" class="btn btn-primary">Update Update Information</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
