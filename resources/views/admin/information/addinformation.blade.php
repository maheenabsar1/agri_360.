@extends('admin.master')

@section('body')
    <div class="container-fluid">
        @if (Session::get('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{Session::get('message')}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row">
            <h2>Add-Information</h2>
            <div class="col-md-12">
                <form action="{{route('new.information')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label >Information name</label>
                        <input type="text" class="form-control" name="info_name"  placeholder="Information name">
                    </div>
                    <div class="form-group">
                        <label >Information Discription</label>
                        <textarea  class="form-control" name="info_desc" placeholder="Information Discription"></textarea>
                    </div>
                    <div class="form-group">
                        <label >Information Image</label>
                        <input type="file" class="form-control-file" name="info_image" >
                        <img src="" id="profile-img-tag" width="200px"/>
                    </div>
                    <div class="form-group ">
                        <label>Publication status</label>
                        <input type="radio" name="status" value="1" >
                        <label >Published</label>
                        <input type="radio" name="status" value="0" >
                        <label >Unpublished</label>
                    </div>
                    <button type="submit" name="btn" class="btn btn-primary">Add-information</button>
                </form>
            </div>
        </div>

    </div>
@endsection
