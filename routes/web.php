<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/','AgricultureController@index')->name('/');

Route::get('/category','AgricultureController@category')->name('category');
Route::get('/product_details','AgricultureController@product_details')->name('product-details');
Route::get('/product-details/{id}','AgricultureController@productDetails')->name('product_details');
Route::get('/product/checkout','AgricultureController@checkout')->name('checkout');


Route::get('/cart','AgricultureController@cart')->name('cart');
Route::get('/viewInformation','AgricultureController@information')->name('information');
Route::get('/informationDetails/{id}','AgricultureController@informationDetails')->name('information.informationDetails');
Route::get('/productDetails/{id}','InformationController@productDetails')->name('product.productDetails');
Route::post('/informationDetails','CommentController@newComment')->name('information.informationComment');
Route::get('/comment','CommentController@showComment')->name('information.comment');

//Route::group(['prefix'=>'farmer'],function(){
//    Route::get('/login','farmer\LoginController@showLoginForm')->name('farmer.login');
//    Route::post('/login','farmer\LoginController@login')->name('farmer.login.submit');
//});
Route::get('/userlogin','AgricultureController@userlogin')->name('userlogin');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/home', 'farmer\LoginController@showFarmerHome')->name('Farmerhome');


Route::get('/category/add', 'CategoryController@index')->name('addcategory');

Route::post('/category/new', 'CategoryController@newcategory')->name('newcategory');

Route::get('/category/view', 'CategoryController@viewcategory')->name('viewcategory');

Route::get('/category/published/{id}','CategoryController@publishedCategory')->name('published-category');
Route::get('/category/unpublished/{id}','CategoryController@unpublishedCategory')->name('unpublished-category');

Route::post('/category/view','CategoryController@updateCategory')->name('update-category');
Route::get('/category/delete/{id}','CategoryController@deleteCategory')->name('delete-category');

Route::get('/information/add', 'InformationController@addInformation')->name('add.information');
Route::post('/information/new', 'InformationController@newInformation')->name('new.information');
Route::get('/information', 'InformationController@index')->name('showinformation');



Route::get('/information/published/{id}','InformationController@publishedInformation')->name('published-information');
Route::get('/information/unpublished/{id}','InformationController@unpublishedInformation')->name('unpublished-information');


Route::post('/information/view','InformationController@updateInformation')->name('update-information');
Route::get('/information/delete/{id}','InformationController@deleteInformation')->name('delete-information');

Route::get('/addProduct',function(){
    return view('front-end.farmer.addProductInformation');
});
Route::group(['prefix'=>'farmer'],function(){
    Route::get('/login','farmer\LoginController@showLoginForm')->name('farmer.login');
    Route::post('/login','farmer\LoginController@login')->name('farmer.login.submit');
    Route::get('/registration','farmer\LoginController@showRegisterForm')->name('farmer.register');
    Route::post('/register','farmer\LoginController@registerFarmer')->name('farmer.register.save');
    Route::get('logout','farmer\LoginController@logout')->name('farmer.logout');
});
Route::get('/addProduct','FarmerController@showProductForm')->name('product.home');

Route::get('/ProductInformation/add', 'FarmerController@addProductInformation')->name('addinformation');
Route::post('/ProductInformation/new', 'FarmerController@newProductInformation')->name('newinformation');
//Route::post('/information', 'FarmerController@store')->name('allinformation');

Route::get('/viewProducts', 'AgricultureController@products')->name('products.show');
