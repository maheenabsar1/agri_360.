<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    //
    public function comments(){
        return $this->hasMany(Comment::class,'info_id');
    }
}
