<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class farmerNotify extends Notification
{
    use Queueable;

    public $information;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($information)
    {
        $this->information= $information;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('New Information Posted')
                    ->greeting('Hello Dear Farmer')
                    ->line('A new Agricultural information added!!')
                    ->line('Information Title: '.$this->information->info_name)
                    ->action('Notification Action', url(route('showinformation', $this->information->info_name)))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
