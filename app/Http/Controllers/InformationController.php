<?php

namespace App\Http\Controllers;

use App\Farmer;
use App\Information;
use App\Notifications\farmerNotify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Image;

class InformationController extends Controller
{

    public function index(){
        $data['information'] = Information::all();
        return view('admin.information.viewinformation',$data);
    }
  public function addInformation()
  {
      return view('admin.information.addinformation');
  }

  public  function  newInformation(Request $request){

        $request->validate([
            'info_name' =>'required',
            'info_desc' =>'required',
            'info_image' =>'required',
            'status' =>'required',
        ]);

      $infoImage = $request->file('info_image');
      $imageName  = $infoImage->getClientOriginalName();
      $directory  = 'public/admin/img/info_images/';
      $imageUrl   = $directory.$imageName;
//     $brandImage->move($directory, $imageName);
      Image::make($infoImage)->resize(400,400)->save($imageUrl);

      $information = new Information();
      $information->info_name = $request->info_name;
      $information->info_desc = $request->info_desc;
      $information->status = $request->status;
      $information->info_image = $imageUrl;

//      Email Area
      $farmers=Farmer::all();
      foreach ($farmers as $farmer){
          Notification::route('mail',$farmer->email)->notify(new farmerNotify($information));
      }

      $information->save();
      return view('admin.information.addinformation')->with('message','Information Added Successfully');
  }

    public function productDetails($id){
        $productDetails=Farmer::findOrFail($id);
        //  dd($productDetails);
        return view('front-end.product.productDetails',['productDetails'=>$productDetails]);

    }

}
