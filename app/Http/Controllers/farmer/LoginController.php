<?php

namespace App\Http\Controllers\farmer;

use App\Farmer;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:farmer')->except('logout');
    }
    public function showFarmerHome(){
        return view('front-end.home');
    }
    public function showLoginForm()
    {
        return view('front-end.farmer.login');
    }
    public function showRegisterForm()
    {
        return view('front-end.farmer.farmerRegister');
    }

    public function registerFarmer(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:farmers',
            'password' => 'required|confirmed',
        ]);
         $farmer = new Farmer();
         $farmer->name = $request->name;
         $farmer->email = $request->email;
         $farmer->password = bcrypt($request->password);
         $farmer->save();
        return redirect()->route('farmer.logout');
    }
    public function login(Request $request){
        $this->validate($request,[
            'email'=>'required|string',
            'password'=>'required|string',
        ]);
        $credentials=[
            'email'=>$request->email,
            'password'=>$request->password,
        ];
        if(Auth::guard('farmer')->attempt($credentials,$request->remember)){

            return redirect()->intended(route('product.home'));
        }
        return redirect()->back()->withInput($request->only('email,remember'));
    }
    public function logout(Request $request){
          Auth::guard('farmer')->logout();
          return redirect('/');
    }
}
