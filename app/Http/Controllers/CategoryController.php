<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
   public function index(){
       return view('admin.category.addcategory');
 }
 public function newcategory(Request $request){
     $category           = new Category();
     $category->cat_name = $request->cat_name;
     $category->cat_desc = $request->cat_desc;
     $category->status   = $request->status;
     $category->save();

     return redirect('/category/add')->with('message','Category Added Successfully');
 }
 public function viewcategory()
 {
     $categories = Category::all();

     return view('admin.category.viewcategory',['categories'=>$categories]);
 }
    public function publishedCategory($id)
    {

        $category = Category::find($id);
        $category->status = 0;
        $category->save();

        return redirect('/category/view');
    }
    public function unpublishedCategory($id)
    {

        $category = Category::find($id);
        $category->status = 1;
        $category->save();

        return redirect('/category/view');
    }
    public function updateCategory(Request $request)
    {
        $category           = Category::find($request->id);
        $category->cat_name = $request->cat_name;
        $category->cat_desc = $request->cat_desc;
        $category->status   = $request->status;
        $category->save();

        return redirect('/category/view')->with('message','Category Update Successfully');
    }
    public function deleteCategory($id){
        $category = Category::find($id);
        $category->delete();

        return redirect('/category/view')->with('message','Category Deleted Successfully');
    }
}
