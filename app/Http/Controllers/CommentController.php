<?php

namespace App\Http\Controllers;
use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index(){
        return view('front-end.information.informationDetails');
    }
    public function newComment(Request $request){

        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'comment'=>'required',
            'info_id'=>'required',
        ]);
        $comment = new Comment();
        $comment->name = $request->name;
        $comment->email = $request->email;
        $comment->comment   = $request->comment;
        $comment->info_id   = $request->info_id;
        $comment->save();

        return redirect()->back()->with('message','Comment Added Successfully');
    }
    public function showComment(){
        $data['comment'] = Comment::orderby('id','desc')->get();

        return view('front-end.information.informationDetails',$data);
    }
}
