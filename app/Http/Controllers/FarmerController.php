<?php

namespace App\Http\Controllers;

use App\Farmer;
use App\Product;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class FarmerController extends Controller
{
   public function __construct(){
       $this->middleware('auth:farmer');
}
    public function showProductForm(){
        return view('front-end.farmer.addProductInformation');
    }
    public function addProductInformation()
    {
        return view('front-end.farmer.addProductInformation');
    }

    public  function  newProductInformation(Request $request){
        $request->validate([
            'product_name' =>'required',
            'product_desc' =>'required',
            'product_image' =>'required',
            'status' =>'required',
            'product_price' =>'required',
            'product_quantity' =>'required',
        ]);

        $infoImage = $request->file('product_image');
        $imageName  = $infoImage->getClientOriginalName();
        $directory  = 'public/admin/img/product_images/';
        $imageUrl   = $directory.$imageName;
        Image::make($infoImage)->resize(400,400)->save($imageUrl);

        $product = new Product();
        $product->product_name = $request->product_name;
        $product->product_desc = $request->product_desc;
        $product->product_quantity = $request->product_quantity;
        $product->product_price = $request->product_price;
        $product->status = $request->status;
        $product->product_image = $imageUrl;
        $product->save();

        return view('front-end.farmer.addProductInformation')->with('message','Information Added Successfully');
    }


}
