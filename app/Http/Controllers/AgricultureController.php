<?php

namespace App\Http\Controllers;

use App\Information;
use Illuminate\Http\Request;
use App\Product;

class AgricultureController extends Controller
{

    public  function index(){

        $newInformations = Information:: orderBy ('id','DESC')->take(3)->get();
        $newProducts = Product::orderBy('id','DESC')->take(3)->get();
        return view('front-end.home.home',[
            'newInformations'=> $newInformations,
            'newProducts'    => $newProducts
        ]);

    }
    public  function category(){

        return view('front-end.category.category');

    }
     public function product_details(){

        return view('front-end.product.product_details');
     }

    public function checkout(){

        return view('front-end.product.checkout');
    }

    public function cart(){

        return view('front-end.cart.cart');
    }
    public function information(){
        $data['information'] = Information::orderby('id','desc')->get();

        return view('front-end.information.information',$data);
    }
    public function products(){

        $data['information'] = Product::orderby('id','desc')->get();
        return view('front-end.product.products',$data);
    }

    public function home(){

        return view('front-end.home.home');
    }
    public function informationDetails($id){
       $infoDetails=Information::with('comments')->where('id',$id)->first();

        return view('front-end.information.informationDetails',['infoDetails'=>$infoDetails]);

    }
    public function productDetails($id){
        $productDetails = Product::all();
        return redirect('front-end.product.productDetails',[
            'productDetails' => $productDetails
        ]);
    }

}
